<?php

namespace App\Services;

use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserService
{
    private $doctrine;

    public function __construct(
        ManagerRegistry $doctrine
    ){
        $this->doctrine = $doctrine;
    }

    public function create_user(
        string $login,
        string $password,
        string $niceName = ''
    ) {
        $user = new User();
        $user->setLogin($login);
        $user->setPassword($password);
        $user->setNiceName($niceName);
        $em = $this->doctrine->getManager();
        $em->persist($user);
        $em->flush();
        return $user->getId();
    }
}
