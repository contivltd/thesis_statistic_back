<?php

namespace App\CommandBus\User;

use App\CommandBus\User\CreateUserCommand;
use App\Repository\UserRepository;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class CreateUserHandler{
    private $em;

    public function __construct(EntityManager $em){
        $this->em = $em;
    }

    public function handle(CreateUserCommand $createUserCommand){

        $user = new User();
        $user->setLogin($createUserCommand->getLogin());
        $user->setPassword($createUserCommand->getPassword());

        //$entityManager =$this->em->getDoctrine()->getManager();
        $this->em->persist($user);
        $this->em->flush();

        dd($user, $createUserCommand->getLogin(), $createUserCommand->getPassword());
    }

}
