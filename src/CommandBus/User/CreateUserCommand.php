<?php

namespace App\CommandBus\User;

class CreateUserCommand{

    private $login;
    private $password;

    public function __construct(
        string $login,
        string $password
    ){
        $this->login = $login;
        $this->password = $password;
    }

    public function getLogin(){
        return $this->login;
    }

    public function getPassword(){
        return $this->password;
    }

}
