<?php

namespace App\Repository;

use App\Entity\QuestionsSet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method QuestionsSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionsSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionsSet[]    findAll()
 * @method QuestionsSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionsSetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionsSet::class);
    }

    // /**
    //  * @return QuestionsSet[] Returns an array of QuestionsSet objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionsSet
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
