<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_datetime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Researcher", inversedBy="projects")
     */
    private $Researcher;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionsSet", mappedBy="Project")
     */
    private $questionsSets;

    public function __construct()
    {
        $this->questionsSets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreateDatetime(): ?\DateTimeInterface
    {
        return $this->create_datetime;
    }

    public function setCreateDatetime(\DateTimeInterface $create_datetime): self
    {
        $this->create_datetime = $create_datetime;

        return $this;
    }

    public function getResearcher(): ?Researcher
    {
        return $this->Researcher;
    }

    public function setResearcher(?Researcher $Researcher): self
    {
        $this->Researcher = $Researcher;

        return $this;
    }

    /**
     * @return Collection|QuestionsSet[]
     */
    public function getQuestionsSets(): Collection
    {
        return $this->questionsSets;
    }

    public function addQuestionsSet(QuestionsSet $questionsSet): self
    {
        if (!$this->questionsSets->contains($questionsSet)) {
            $this->questionsSets[] = $questionsSet;
            $questionsSet->setProject($this);
        }

        return $this;
    }

    public function removeQuestionsSet(QuestionsSet $questionsSet): self
    {
        if ($this->questionsSets->contains($questionsSet)) {
            $this->questionsSets->removeElement($questionsSet);
            // set the owning side to null (unless already changed)
            if ($questionsSet->getProject() === $this) {
                $questionsSet->setProject(null);
            }
        }

        return $this;
    }
}
