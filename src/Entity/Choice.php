<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChoiceRepository")
 */
class Choice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Respondent", inversedBy="choices")
     */
    private $Respondent;

    /**
     * @ORM\Column(type="datetime")
     */
    private $create_datetime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Option", inversedBy="choices")
     */
    private $selectedOption;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRespondent(): ?Respondent
    {
        return $this->Respondent;
    }

    public function setRespondent(?Respondent $Respondent): self
    {
        $this->Respondent = $Respondent;

        return $this;
    }

    public function getCreateDatetime(): ?\DateTimeInterface
    {
        return $this->create_datetime;
    }

    public function setCreateDatetime(\DateTimeInterface $create_datetime): self
    {
        $this->create_datetime = $create_datetime;

        return $this;
    }

    public function getSelectedOption(): ?Option
    {
        return $this->selectedOption;
    }

    public function setSelectedOption(?Option $selectedOption): self
    {
        $this->selectedOption = $selectedOption;

        return $this;
    }
}
