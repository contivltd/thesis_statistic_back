<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Services\UserService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use League\Tactician\CommandBus;
use App\CommandBus\User\CreateUserCommand;

class UserController extends AbstractController
{
    private $userService;
    private $userRepository;
    private $commandBus;

    public function __construct(
        UserService $userService,
        UserRepository $userRepository,
        CommandBus $commandBus
    ){
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/users", methods={"GET"}, name="user_create")
     */
    public function create(Request $request)
    {
        $login = 'jakiś_login';
        $password = 'jakies_haselo_trudne_w_huj';
        $command = new CreateUserCommand($login, $password);
        $this->commandBus->handle($command);
        return $this->json([
            'id' => null,
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }









    /**
     * @Route("/users/a", methods={"GET"}, name="users_list")
     */
    public function list()
    {
        $command = new CreateUserCommand('dupa', 'dupa2');
        $this->commandBus->handle($command);
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    /**
     * @Route("/users/{id}", name="users_get_single")
     */
    public function single(int $id, Request $request)
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    public function search()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    public function getSingle(int $id, Request $request)
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }



    public function edit(int $id, Request $request)
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

    public function delete(int $id, Request $request)
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/UserController.php',
        ]);
    }

}
